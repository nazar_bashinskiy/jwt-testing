const AuthModule = require('./src/auth/auth.module');
const TestModule = require('./src/test/test.module');

module.exports = function (app){
    AuthModule(app);
    TestModule(app);
}