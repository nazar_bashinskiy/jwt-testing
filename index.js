const dotenv = require('dotenv');
const express = require('express');
const AppModule = require('./app.module');
const AppConfugure = require('./app.configure');

dotenv.config();

const app = express();

AppConfugure(app);
AppModule(app);

app.listen(process.env.PORT, () => console.log('app started' + process.env.PORT));