const { authenticate } = require('passport');
const TestService = require('./test.service');

const testService = new TestService();

module.exports = function (app){
    app.get('/test', testService.authenticate('jwt'), (req, res) => {
        res.send(req.user);
    });
}