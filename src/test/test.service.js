require('dotenv').config();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const PassportJwtStrategyConfig = require('../passport/passport.jwt-strategy.config');

const AuthRepository = require('../auth/auth.repository');

module.exports = class TestService{
    constructor(){
        this.authRepository = new AuthRepository();
        PassportJwtStrategyConfig(passport, this.authRepository);
    }

    authenticate(strategy){
        return passport.authenticate(strategy);
    }
}