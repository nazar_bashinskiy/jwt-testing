require('dotenv').config();
const jwtStrategy = require('passport-jwt');

module.exports = function(passport, repository){
    passport.use(new jwtStrategy.Strategy({
        jwtFromRequest: jwtStrategy.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.jwtSecret
    }, (jwtPayload, done) => {
        const user = repository.findUserByLogin(jwtPayload.login);
        delete user.password;
        
        if (!user)
            return done(null, false, { message: 'Incorrect username.' });

        else 
            return done(null, user);
    }));
}