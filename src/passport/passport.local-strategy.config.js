const LocalStrategy = require('passport-local');

module.exports = function(passport, repository){
    passport.serializeUser((user, done) => {
        done(null, user);
    });
      
    passport.deserializeUser((user, done) => {
       done(null, user);
    });
    
    passport.use(new LocalStrategy((username, password, done) => {
        const user = repository.findUserByLogin(username);
        if (!user)
            return done(null, false, { message: 'Incorrect username.' });
        
        else if (user.password != password)
            return done(null, false, { message: 'Invalid username.' });
        
        else 
            return done(null, user);
    }));
}