module.exports = {
    name: 'Users',
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: true
        },

        login: {
            type: 'varchar'
        },
        password: {
            type: 'varchar'
        }
    }
};