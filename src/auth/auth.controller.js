require('dotenv').config();
const AuthService = require('./auth.service');

const authService = new AuthService();

const jwt = require('jsonwebtoken');

module.exports = function (app) {
    app.post('/auth', authService.authenticate('local'), (req, res) => {
        res.send(jwt.sign(req.user, process.env.jwtSecret));
    });
}