require('dotenv').config();
const AuthRepository = require('./auth.repository');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const PassportLocalStrategyConfig = require('../passport/passport.local-strategy.config');

module.exports = class AuthService {
    constructor(){
        this.authRepository = new AuthRepository();
        PassportLocalStrategyConfig(passport, this.authRepository);
    }

    authenticate(strategy){
        return passport.authenticate(strategy);
    }

}